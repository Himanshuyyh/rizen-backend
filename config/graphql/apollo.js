const { ApolloServer } = require("apollo-server-express");

const { ErrorHandler } = require("../../helpers/errors");
const { typeDefs, resolvers } = require("./graphql");
const directives = require("./directives");

module.exports = new ApolloServer({
    typeDefs,
    resolvers,
    formatError: ErrorHandler,
    schemaDirectives: directives,
    tracing: true,
    context: ({ req, res }) => ({ req, res }),
});
