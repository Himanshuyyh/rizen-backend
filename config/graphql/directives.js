const AuthenticatedDirective = require("../../directives/authenticated");

module.exports = {
    authenticated: AuthenticatedDirective,
};
