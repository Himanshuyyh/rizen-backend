const path = require("path");
const { loadFilesSync } = require("@graphql-tools/load-files");
const { mergeTypeDefs, mergeResolvers } = require("@graphql-tools/merge");

const loader = (pattern) => {
    return loadFilesSync(path.join(process.env.APPPATH, pattern));
};

const types = loader("modules/**/*.graphql");
const resolvers = loader("modules/**/*.resolvers.js");
module.exports = {
    typeDefs: mergeTypeDefs(types, { all: true }),
    resolvers: mergeResolvers(resolvers),
};
