const mongoose = require("mongoose");
const Quiz = require("../models/quiz");
const quizId = new mongoose.Types.ObjectId()
const questionId = new mongoose.Types.ObjectId()
const optionId = new mongoose.Types.ObjectId()
const User = require("../models/user");
const jwt = require('jsonwebtoken')
const userOneId = new mongoose.Types.ObjectId()
const token = jwt.sign({
    userId: userOneId
}, process.env.CLIENT_SECRET, { expiresIn: "1h" });
const userOne = {
    name: "test",
    email: "test@gmail.com",
    password: "12345678",
    _id: userOneId
}

const quizOne = {

    "_id": quizId,
    "time": "20",
    "topic": "tech",
    "questions": [
        {
            "preferredJobRoles": ["REACT", "NODE"],
            "filePath": [{ fileName: "abcd", path: "somepath" }],
            "essay": "some big text",
            "_id": questionId,
            "question": "what is react",
            "inputType": "mcq",
            "options": [
                {
                    "weight": 50,
                    "_id": optionId,
                    "option": "react is frontend"
                },

            ],
            "weightAge": 10
        },

    ],


}
const quizCollection = async () => {
    await Quiz.deleteMany();
    await new Quiz(quizOne).save(); await User.deleteMany();
    await new User(userOne).save();
};

module.exports = {
    quizCollection,
    quizId,
    optionId,
    questionId, token
};
