const mongoose = require("mongoose");
const HeadCount = require("../models/headCount");
const objectId = new mongoose.Types.ObjectId()
const headCountOne = {
    _id: objectId,
    postName: "REACT",
    candidateCount: 2,
    createdBy: "607424a2b9cf533f4c6fba56",
    priority: "Medium",
}

const headCountCollection = async () => {
    await HeadCount.deleteMany();
    await new HeadCount(headCountOne).save();
};

module.exports = {
    headCountCollection,
    objectId,

};
