const mongoose = require("mongoose");
const jdKeyword = require("../models/jdKeywords");
const Location = require('../models/location')
const JobDescriptionTemplate = require("../models/JobDescriptionTemplate");
const objectId = new mongoose.Types.ObjectId()
const User = require("../models/user");
const jwt = require('jsonwebtoken')
const userOneId = new mongoose.Types.ObjectId()
const token = jwt.sign({
  userId: userOneId
}, process.env.CLIENT_SECRET, { expiresIn: "1h" });
const userOne = {
  name: "test",
  email: "test@gmail.com",
  password: "12345678",
  _id: userOneId
}


const jobDescriptionTemplateOne = {
  _id: objectId,
  plannedDate: true,
  keywords: [
    "NODEJS"
  ],
  skills: [
    "abcd"
  ],
  workFromHome: false,
  questionnaire: false,
  autoScreening: false,
  status: "DRAFT",
  title: "dragon",
  location: "loc",
  employmentType: "Full time",
  workSchedule: "Other",
  competences: "12 lakh",
  candidateRole: "react",
  candidateResponsibilities: "frontend",
  requirement: [
    {

      requirementType: "education",
      requirementValue: "BE",
      requirementSpecification: "node",
      preferredType: "preferred"
    },
    {

      requirementType: "experience",
      requirementValue: "1 year",
      requirementSpecification: "react",
      preferredType: "preferred"
    }
  ],
  "createdBy": "603cacc6cc7cf01e14017aa4",

}



const jdtCollection = async () => {
  await JobDescriptionTemplate.deleteMany()
  await new JobDescriptionTemplate(jobDescriptionTemplateOne).save()
  await Location.deleteMany()
  await new Location({ location: 'Bangalore' }).save()
  await User.deleteMany();
  await new User(userOne).save();

};

module.exports = {
  jdtCollection,
  objectId, token
};
