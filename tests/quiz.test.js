const app = require("../app");
const { quizId, questionId, optionId, token, quizCollection } = require('./quizDummy')
const supertest = require("supertest");
const request = supertest(app);
beforeEach(quizCollection);
test("create quiz", async () => {

    const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
        query: `mutation {
    createQuiz(
      data: { time: "20", topic: "tech"

      questions: [
        {
          question: "what is react"
          inputType: "mcq"

          filePath: [{ fileName: "abcd", path: "somepath" }]
          options: [
            { option: "react is frontend", weight: 50 }
            { option: "react is backend", weight: 50 }
            { option: "a", weight: 50 }
          ]

          weightAge: 10
        }
        {
          question: "what is node"
          inputType: "mcq"
          options: [
            { option: "node is frontend", weight: 50 }
            { option: "node is backend", weight: 50 }
            { option: "a", weight: 50 }
          ]
          weightAge: 50
        }
      ]
      }) {
    success
    }
  }

      `});

    expect(response.body.data).toStrictEqual({
        createQuiz: {
            success: true
        }
    });
});

// test("get quiz list", async () => {
//   const response = await request.post("/graphql").send({
//     query: ` {
//     getQuiz {
//       questions {
//         options {
//           weight
//           option
//         }
//       }
//       time
//       topic
//       _id
//     }
//   }


//   `});

// //   //   expect(response.body.data).toStrictEqual({
// //   //     createQuiz: {
// //   //       questions: [
// //   //         {
// //   //           question: "what is react"
// //   //         },
// //   //         {
// //   //           question: "what is node"
// //   //         }
// //   //       ]
// //   //     }
// //   //   }
// //   // )
// // });

test("get quiz by id ", async () => {
    const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
        query: `query user($quizId: ID!) {
  getQuiz(quizId:$quizId)
  {
    topic
    time
    questions{
     question
     options{
weight
option
     }
    }
  }   }
  `, variables: {
            quizId
        }
    });

    console.log("sdd", response.body)
    expect(response.body.data).toStrictEqual({
        getQuiz: {
            time: "20",
            topic: "tech",
            questions: [
                {
                    question: "what is react",
                    options: [
                        {
                            weight: 50,

                            option: "react is frontend"
                        },

                    ],
                },

            ]
        }
    }
    )

});

test("delete quiz", async () => {
    const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
        query: `mutation user($quizId: ID!) {

        deleteQuiz(quizId:$quizId)
          {success}
    }
    `, variables: {
            quizId
        }
    });
    expect(response.body.data).toStrictEqual({
        deleteQuiz: {
            success: true
        }
    })
    // console.log("%j",response.body.data)

});






// test("delete question", async () => {
//   const response = await request.post("/graphql").send({
//     query: `mutation user($quizId: ID!,$questionId:ID!) {
//         deleteQuestion(quizId:$quizId,questionId:$questionId)
//         {success}
//       }
//       `, variables: {
//       quizId,
//       questionId
//     }
//   });


//   expect(response.body.data).toStrictEqual({
//     deleteQuestion: {
//       success: true
//     }
//   })


// });



