const app = require("../app");
const { jdtCollection, objectId, token } = require("./jobDescriptionDummy");
const supertest = require("supertest");
const request = supertest(app);
beforeEach(jdtCollection);
test("create jd", async () => {
  console.log("token", token)
  const response = await request.post("/graphql")
    .set('Authorization', `Bearer ${token}`).send({
      query: `
  mutation {
    jobDescriptionCreate(
      jobDescriptionData: {
       title:"ram"
        skills:["abcd"]
        createdBy:"603cacc6cc7cf01e14017aa4"
        location: "loc"
        competences:"12 lakh"
        workSchedule: "Other"
        keywords: ["NODEJS"]
        employmentType: "Full time"
        candidateRole: "react"
        jobDate:"22-12-2021"
        plannedDate:true
        candidateResponsibilities: "frontend"
       requirement :[
       { requirementType:"education"
        requirementValue:"BE"
        requirementSpecification:"node"
        preferredType:"preferred"
         },

      { requirementType:"experience"
        requirementValue:"1 year"
        requirementSpecification:"react"
        preferredType:"preferred"

      },


      ]
      status:"DRAFT"
      }

    ) {
      success
    }
  }


      `});


  expect(response.body.data).toStrictEqual({
    jobDescriptionCreate: {
      success: true
    }
  });
});

test("edit jd", async () => {
  console.log("id", objectId)
  const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({

    query: `mutation user($objectId: ID!) {
            jobDescriptionEdit(
              jobDescriptionId: $objectId
              jobDescriptionEdit: {
                title:"dragon"
                skills:["abcd"]
                createdBy:"603cacc6cc7cf01e14017aa4"
                location: "loc"
                competences:"12 lakh"
                workSchedule: "Other"
                keywords: ["NODEJS"]
                employmentType: "Full time"
                candidateRole: "react"
                jobDate:"22-12-2021"
                plannedDate:true
                candidateResponsibilities: "frontend"
               requirement :[
               { requirementType:"education"
                requirementValue:"BE"
                requirementSpecification:"node"
                preferredType:"preferred"
                 },

              { requirementType:"experience"
                requirementValue:"1 year"
                requirementSpecification:"react"
                preferredType:"preferred"

              },


              ]
              status:"DRAFT"
              }

            ) {
              success
            }
          }

              `, variables: {
      objectId
    }
  });
  console.log(response.body);
  expect(response.body.data).toStrictEqual({
    jobDescriptionEdit: {
      success: true
    }
  });
});

test("delete jd", async () => {
  const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
    query: `mutation user($objectId: ID!) {
    jobDescriptionDelete(jobDescriptionId:$objectId)
    {success}
  }
              `, variables: {
      objectId
    }
  });
  console.log(response.body.errors);
  expect(response.body.data).toStrictEqual({
    jobDescriptionDelete: {
      success: true
    }
  });
});

test("location list", async () => {
  const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
    query: `query
    {
      jdLocationList{
        location
      }
    }

              `
  });
  expect(response.body.data).toStrictEqual({ jdLocationList: [{ location: 'Bangalore' }] })
});


test("get jd by id", async () => {
  const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
    query: `

  query user($objectId: ID!) {

      getJobDescription(jobDescriptionId:$objectId){
      skills
      status
      keywords

    }
  }
              `, variables: {
      objectId
    }
  });

  expect(response.body.data).toStrictEqual({
    "getJobDescription": {
      "skills": [
        "abcd"
      ],
      "status": "DRAFT",
      "keywords": [
        "NODEJS"
      ]
    }
  });
});

test("list jd", async () => {
  const response = await request.post("/graphql").set('Authorization', `Bearer ${token}`).send({
    query: `
  query
  {
    jobDescriptionTemplateList(filter:{skip:0,limit:1}){
      jobDescriptionTemplate{
          keywords
        title
        employmentType
        workSchedule
        requirement{
          requirementType
        }
      }
      count
    }
  }
              `});
  const { data: { jobDescriptionTemplateList: { jobDescriptionTemplate } } } = response.body

  expect(jobDescriptionTemplate.length).toEqual(1)
});
