const mongoose = require("mongoose");
const Constants = require("../helpers/constants");
const headCountSchema = new mongoose.Schema({
  // job required
  postName: {
    type: String,
    required: true
  },
  // number of candidate required
  candidateCount: {
    type: String,
    required: true
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  description: {
    type: String
  },
  //status of headcount pending approved denied
  status: {
    type: String,
    default: Constants.headCountStatus.pending,
    enum: [Constants.headCountStatus.pending, Constants.headCountStatus.approved, Constants.headCountStatus.decline]
  },
  priority: {
    type: String,
    enum: [Constants.headCountPriority.low, Constants.headCountPriority.medium, Constants.headCountPriority.high],
    default: Constants.headCountStatus.low

  }
}, { timestamps: true });
const location = mongoose.model("headCount", headCountSchema);
module.exports = location;
