const mongoose = require("mongoose");
const Constants = require("../helpers/constants");
const assessmentSchema = new mongoose.Schema({
  //which technology test to be taken
  topic: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: Constants.statusType.draft,
    enum: [Constants.statusType.published, Constants.statusType.draft]
  },
  category: {
    type: String
  },
  preferredJobRoles: [String],
  subCategory: {
    type: String
  },
  // internal test time
  time: {
    type: String
    // required: true
  },

  questions: [
    {
      question: {
        type: String,
        required: true
      },

      filePath: [{
        fileName: String,
        path: String
      }],
      //type of input for candidate eg multiple choice ,input box
      inputType: {
        type: String,
        required: true
      },
      //used to decide if single weight or multiple weight
      distributedOptionToggle: Boolean,
      shuffle: Boolean,
      essay: String,
      options: [
        {
          weight: {
            type: Number,
            default: 100
          },
          option: String
        }
      ],
      weightAge: {
        type: Number,
        // required: true
      }
    }
  ],
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },

}, {
  timestamps: {
    createdAt: "createdAt",
    updatedAt: "updatedAt"
  }
});
assessmentSchema.index({ topic: "text", time: "text", preferredJobRoles: "text" });

const Quiz = mongoose.model("Assessment", assessmentSchema);
module.exports = Quiz;
