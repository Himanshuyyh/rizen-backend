const mongoose = require("mongoose");
const validator = require('validator')
const bcrypt = require('bcrypt')

const employmentSchema = new mongoose.Schema({
    email: {
        type: String,

        // required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    status: { type: String },
    dateOfBirth: { type: String },
    fatherName: { type: String },
    address: { type: String },
    panID: { type: String },
    AadharCardNo: { type: String },
    mobileNumber: { type: Number },
    landLineNumber: { type: Number },
    passportNumber: { type: Number },
    emergencyContact: { type: Number },
    employeeQualification: { type: String },
    departmentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Department"
    },
    roleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
    }

}, {
    timestamps: true
})

employmentSchema.pre('save', async function (next) {
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

const Employee = mongoose.model("Employee", employmentSchema);
module.exports = Employee;
