const mongoose = require("mongoose");
const roleSchema = new mongoose.Schema({
  permission: {
    key: String,
    readAccess: {
      type: Boolean,
      default: false
    },
    writeAccess: {
      type: Boolean,
      default: false
    },
    updateAccess: {
      type: Boolean,
      default: false
    },
    deleteAccess: {
      type: Boolean,
      default: false
    }
  },
  name: String
});
const roles = mongoose.model("Role", roleSchema);
module.exports = roles;
