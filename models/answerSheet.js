const mongoose = require("mongoose");
const answerSheet = new mongoose.Schema({
  answers: [
    {
      questionId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Quiz"
      },

      optionId: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Quiz"
        }
      ]
    }
  ],

  candidateId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Candidate"
  },
  quizId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Quiz"
  },
  score: {
    type: Number,
    default: 0
  }
});
const Quiz = mongoose.model("AnswerSheet", answerSheet);
module.exports = Quiz;