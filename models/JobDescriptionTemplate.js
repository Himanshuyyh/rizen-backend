const mongoose = require("mongoose");
const Constants = require("../helpers/constants");
const jobDescriptionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,

  },
  location: {
    type: String,
    required: true,


  },

  requirement: [
    {
      requirementType: String,
      requirementValue: String,
      requirementSpecification: String,
      preferredType: {
        type: String,
        enum: ["preferred", "required"]
      }
    }
  ],

  plannedDate: {
    type: Boolean,
    default: false
  },
  jobDate: String,

  keywords: {
    type: [String],
    required: true,
  },
  skills: {
    type: [String],
    required: true
  },

  employmentType: {
    type: String,
    required: true,
    enum: ["Full time", "Part time", "Work from home", "On site", "Internship"]
  },


  workSchedule: {
    type: String,
    required: true,
    enum: ["Day Shift", "Night Shift", "Rotational Shift", "Other"]
  },
  candidateRole: {
    type: String,
    required: true
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  workFromHome: {
    type: Boolean,
    default: false
  },
  questionnaire: {
    type: Boolean,
    default: false
  },
  autoScreening: {
    type: Boolean,
    default: false
  },

  candidateResponsibilities: {
    type: String,
    required: true
  },
  competences: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: Constants.statusType.draft,
    enum: [
      Constants.statusType.published, Constants.statusType.draft
    ],
    default: Constants.statusType.draft
  },

}, {
  timestamps: {
    createdAt: "createdAt",
    updatedAt: "updatedAt",

  }
});
jobDescriptionSchema.index({ title: "text", location: "text", keywords: "text" });
const jobDescription = mongoose.model("JobDescriptionTemplate", jobDescriptionSchema);
module.exports = jobDescription;
