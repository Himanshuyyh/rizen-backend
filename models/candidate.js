const mongoose = require("mongoose");
const candidateSchema = new mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }

}, {
  timestamps: true
});
const candidate = mongoose.model("Candidate", candidateSchema);
module.exports = candidate;