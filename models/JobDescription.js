const mongoose = require("mongoose");
const Constants = require("../helpers/constants");
const jobDescriptionSchema = new mongoose.Schema({
  jdTemplateId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "JobDescriptionTemplate"
  },
  assessmentId: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Assessment"
    }
  ],
  headCountId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "headCount"
  },
  candidateQualification: { type: Boolean, default: false },
  screeningFilter: { type: Boolean, default: false },
  startDate: { type: String },
  documents: [{
    document: String,
    preferredType: {
      type: String,
      enum: ["preferred", "required"]
    }
  }],
  requirement: [
    {
      requirementType: String,
      requirementValue: String,
      preferredType: {
        type: String,
        enum: ["preferred", "required"]
      }
    }
  ],
  jdStatus: {
    type: String,
    default: Constants.headCountStatus.pending,
    enum: [Constants.headCountStatus.pending, Constants.headCountStatus.approved, Constants.headCountStatus.decline]
  }, title: {
    type: String,
    //    required: true,

  },
  location: {
    type: String,
    //  required: true,


  },



  plannedDate: {
    type: Boolean,
    default: false
  },
  jobDate: String,

  keywords: {
    type: [String],
    //required: true,
  },
  skills: {
    type: [String],
    //required: true
  },

  employmentType: {
    type: String,
    //required: true,
    enum: ["Full time", "Part time", "Work from home", "On site", "Internship"]
  },


  workSchedule: {
    type: String,
    //required: true,
    enum: ["Day Shift", "Night Shift", "Rotational Shift", "Other"]
  },
  candidateRole: {
    type: String,
    // required: true
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  workFromHome: {
    type: Boolean,
    default: false
  },
  questionnaire: {
    type: Boolean,
    default: false
  },
  autoScreening: {
    type: Boolean,
    default: false
  },

  candidateResponsibilities: {
    type: String,
    //required: true
  },
  competences: {
    type: String,
    // required: true
  },
  topic: {
    type: String,
    //required: true
  },
  category: {
    type: String
  },
  preferredJobRoles: [String],
  subCategory: {
    type: String
  },
  // internal test time
  time: {
    type: String
    // required: true
  },

  questions: [
    {
      question: {
        type: String,
        //  required: true
      },
      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
      },
      updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
      },
      filePath: [String],
      //type of input for candidate eg multiple choice ,input box
      inputType: {
        type: String,
        // required: true
      },
      //used to decide if single weight or multiple weight
      distributedOptionToggle: Boolean,
      shuffle: Boolean,
      essay: String,
      options: [
        {
          weight: {
            type: Number,
            default: 100
          },
          option: String
        }
      ],
      weightAge: {
        type: Number,
        //  required: true
      }
    }
  ],



}, {
  timestamps: true
});
const jobDescription = mongoose.model("JobDescription", jobDescriptionSchema);
module.exports = jobDescription;
