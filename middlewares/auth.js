const jwt = require("jsonwebtoken");
const userService = require(".././modules/admin/services/user.services");
module.exports = async ({ req }) => {
    const header = req.get("Authorization");
    if (!header) return null;
    const token = header.split(" ")[1];
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, process.env.CLIENT_SECRET);
    } catch (err) {
        return null;
    }

    return !decodedToken ? null : await userService.show(decodedToken.userId);

};
