'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      age: {
        type: Sequelize.FLOAT
      },

      email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: { isEmail: true }
      },
      profilePic: {
        type: Sequelize.STRING,

      },
      createdAt: {
        type: Sequelize.DATE

      },
      updatedAt: {
        type: Sequelize.DATE
      },
      departmentId: {
        type: Sequelize.INTEGER,
        references: { model: 'departments', key: 'id' }
      }
    });

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
