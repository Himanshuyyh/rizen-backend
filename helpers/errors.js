const { ApolloError } = require("apollo-server-express");

const formatJoi = errors => {
  const error = errors[0];

  return { key: error.path[0], message: error.message };
};

class ERROR extends Error {
  constructor(statusCode, data, message, error) {
    super();
    this.status = statusCode;
    this.error = error



    if (statusCode === 422 && data.errors) {
      Object.assign(this, formatJoi(data.errors));
    } else {
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          this[key] = data[key];
        }
      }
    }

    if (message) {
      this.message = message;
    }
  }
}

const ErrorHandler = error => {
  if (error.originalError instanceof ApolloError) {

    return error;
  }

  if (error.originalError instanceof ERROR) {

    return {
      ...error.originalError
    };
  }

  return error;
};

module.exports = {
  ERROR: ERROR,
  ErrorHandler
};
