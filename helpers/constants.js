exports.statusType = { published: "PUBLISHED", draft: "DRAFT" }
exports.headCountStatus = { pending: "Pending", approved: "Approved", decline: "Decline" }
exports.headCountPriority = { low: "Low", medium: "Medium", high: "High" }

