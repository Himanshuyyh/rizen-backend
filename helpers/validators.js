const Joi = require("joi");

exports.string = {
    required() {
        return Joi.string().required();
    },
    nullable() {
        return Joi.string().required().allow("", null);
    },
    optional() {
        return this.nullable().optional();
    },
};

exports.any = {
    required() {
        return Joi.any().required();
    },
    nullable() {
        return Joi.any().required().allow("", null);
    },
    optional() {
        return this.nullable().optional();
    },
};

exports.number = {
    required() {
       
        return Joi.number().integer().unsafe().required();
    },
    nullable() {
        return Joi.number().integer().unsafe().required().allow(null, "");
    },
    optional() {
        return this.nullable().optional();
    },
};






exports.mobile = {
    required() {
        return Joi.number()
            .integer()
            .min(1000000000)
            .max(9999999999)
            .required();
    },
    nullable() {
        return Joi.number()
            .integer()
            .min(1000000000)
            .max(9999999999)
            .required()
            .allow("", null);
    },
    optional() {
        return this.nullable().optional();
    },
};

exports.boolean = () => Joi.boolean().required();

exports.email = {
    required() {
        return Joi.string().email().required();
    },
    nullable() {
        return Joi.string().email().required().allow(null, "");
    },
    optional() {
        return this.nullable().optional();
    },
};
