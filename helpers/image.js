BASE_URL = process.env.BASE_URL
const {
    mkdirSync,
    existsSync,
    createWriteStream
} = require('fs');
const { join, parse } = require('path');

const imageUpload = async (file) => {
    try {
        const {
            filename,
            createReadStream
        } = await file;
        let stream = createReadStream();

        let {
            ext,
            name: profilePicName
        } = parse(filename);

        profilePicName = profilePicName.replace(/([^a-z0-9 ]+)/gi, '-').replace(' ', '_');

        let serverFile = join(
            `${process.env.Dir}/public/${profilePicName}-${Date.now()}${ext}`
        );
        console.log("serfile", serverFile)
        console.log("dir", __dirname)
        serverFile = serverFile.replace(' ', '_');
        let writeStream = await createWriteStream(serverFile);
        await stream.pipe(writeStream);
        console.log(profilePicName)
        console.log(serverFile)
        serverFile = `${serverFile.split('public')[1]}`;
        console.log("serverFile", serverFile)
        return serverFile

    } catch (err) {
        console.log(err)
        // throw new ApolloError(err.message);
    }



};
module.exports=imageUpload