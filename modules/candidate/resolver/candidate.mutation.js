const candidateService = require('../service/candidate.services')
module.exports = {
   candidateCreate: async (parent, { candidateData }) => candidateService.candidateCreate(candidateData),
   editCandidate: async (parent, { candidateId, candidateEdit }) => candidateService.editCandidate(candidateId, candidateEdit),
   candidateDelete: async (parent, { candidateId }) => candidateService.candidateDelete(candidateId)
};
