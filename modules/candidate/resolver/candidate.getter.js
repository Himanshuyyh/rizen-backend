const candidate = require('../../../models/candidate');
const candidateService = require('../service/candidate.services')
module.exports = {
    candidateList: (parent, { filter, searchValue }) => candidateService.candidateList(filter, searchValue),
    getCandidate: (parent, { candidateId }) => candidateService.getCandidate(candidateId)
};
