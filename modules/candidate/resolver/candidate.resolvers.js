const getters = require("./candidate.getter");
const mutations = require("./candidate.mutation");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
