const Candidate = require("../../../models/candidate");
const { ERROR } = require("../../../helpers/errors");


exports.candidateCreate = async candidateData => {
  const candidate = await Candidate.create(candidateData).catch(error => {
    throw new ERROR(400, {
      key: "candidate",
      message: error.message,

    });
  });;
  return { success: !!candidate };
};

exports.candidateList = async (filter, searchValue) => {
  const {
    skip = 0,
    limit = 10,
    sortName = "updatedAt",
    sortType = -1
  } = filter;
  const count = await Candidate.find().countDocuments().catch(error => {
    throw new ERROR(400, {
      key: "jd list",
      message: error.message
    });
  });

  if (searchValue) {
    const candidate = await Candidate.find({
      $text: {
        $search: searchValue
      },

    }).skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
      throw new ERROR(400, {
        key: "jd list",
        message: error.message
      });
    })
    return {
      candidate,
      count
    }
  }
  const candidate = await Candidate.find().skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
    throw new ERROR(400, {
      key: "jd list",
      message: error.message
    });
  });
  return {
    candidate,
    count
  };
};

exports.getCandidate = async candidateId => await Candidate.findById({ _id: candidateId }).catch(error => {
  throw new ERROR(400, {
    key: "candidate",
    message: error.message,

  });
});


exports.candidateDelete = async (candidateId) => {
  const candidate = await Candidate.findByIdAndDelete({ _id: candidateId }).catch(error => {
    throw new ERROR(400, {
      key: "candidate",
      message: error.message
    });
  });

  return { success: !!candidate }
}

exports.editCandidate = async (candidateId, candidateEdit) => {
  const updates = {
    ...candidateEdit
  };
  const candidate = await Candidate.findByIdAndUpdate({
    _id: candidateId
  }, updates).catch(error => {
    throw new ERROR(400, {
      key: "jd update",
      message: error.message
    });
  });
  if (!candidate) {
    throw new ERROR(400, {
      key: "jd update",
      message: "jd not found"
    });
  }

  return {
    success: !!candidate
  };
};

