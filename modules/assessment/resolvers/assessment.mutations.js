const quizService = require("../services/assessment.services");

module.exports = {
  createQuiz: (parent, { data, quizId, }, { USER }) => quizService.createQuiz(data, quizId, USER._id),
  takeQuiz: (parent, { quizId, answers, candidateId, optionId }) => quizService.takeQuiz(quizId, answers, candidateId, optionId),
  answerMedium: (parent, { inputType }) => quizService.answerMedium(inputType),
  editQuiz: (parent, { quizId, quizEdit }, { USER }) => quizService.editQuiz(quizId, quizEdit, USER._id),
  deleteQuiz: (parent, { quizId }, { USER }) => quizService.deleteQuiz(quizId, USER._id),
  deleteQuestion: (parent, { questionId, quizId, question }, { USER }) => quizService.deleteQuestion(questionId, quizId, question, USER._id),
  deleteOption: (parent, { quizId, optionId }, { USER }) => quizService.deleteOption(quizId, optionId, USER._id)

};
