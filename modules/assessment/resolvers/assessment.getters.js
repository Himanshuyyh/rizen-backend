const quizService = require("../services/assessment.services");

module.exports = {

    assessmentList: async (parent, { filter, searchValue }, { USER }) => await quizService.quizList(filter, searchValue, USER._id),
    getQuiz: async (parent, { quizId }, { USER }) => await quizService.getQuizById(quizId, USER._id),
    quizTimeOut: (parent) => quizService.quizTimeOut(),
    answerMediumList: async (parent) => await quizService.answerMediumList()


};
