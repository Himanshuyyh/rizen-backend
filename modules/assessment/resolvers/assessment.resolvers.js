const getters = require("./assessment.getters");
const mutations = require("./assessment.mutations");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
