const Assessment = require("../../../models/quiz");

const AnswerSheet = require("../../../models/answerSheet");
const AnswerMedium = require("../../../models/answerMedium");
const { ERROR } = require("../../../helpers/errors");
//const imageUpload = require("@/helpers/image");
exports.createQuiz = async (data, quizId, userId) => {
  const quizExist = await Assessment.findOneAndUpdate({
    _id: quizId
  }, data).catch(error => {
    throw new ERROR(400, {
      key: "quiz update",
      message: error.message
    });
  });
  //console.log(quizExist.questions)
  if (quizExist)
    return {
      success: !!quizExist,
      id: quizExist._id
    };
  const quiz = await Assessment.create({ ...data, createdBy: userId });
  return {
    success: !!quiz,
    id: quiz._id
  };
};
exports.quizList = async (filter, searchValue, userId) => {
  const { skip = 0,
    limit = 10, sortName = "updatedAt", sortType = -1 } = filter
  const count = await Assessment.find().countDocuments().catch(error => {
    throw new ERROR(400, {
      key: "quiz list",
      message: error.message
    });
  });
  if (searchValue) {
    const assessment = await Assessment.find({
      $text: {
        $search: searchValue
      },

    }).skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
      throw new ERROR(400, {
        key: "quiz list",
        message: error.message
      });
    });

    return {
      count,
      assessment
    }

  }
  const assessment = await Assessment.find().skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
    throw new ERROR(400, {
      key: "quiz list",
      message: error.message
    });
  });

  return {
    count,
    assessment
  };
};
exports.getQuizById = async (quizId, userId) => {
  const quiz = await Assessment.findOne({ _id: quizId }).catch(error => {
    throw new ERROR(400, {
      key: "get quiz",
      message: error.message
    });
  });
  return quiz;
};
exports.takeQuiz = async (quizId, answers, candidateId) => {
  //candidate should take test one

  const candidateExist = await AnswerSheet.findOne({ candidateId: candidateId });
  // if (candidateExist)
  //   throw new ERROR(400, {
  //     key: "candidate",
  //     message: "u already given test"
  //   });
  const answerSheet = new AnswerSheet({ quizId: quizId, candidateId: candidateId });
  answerSheet.answers = answers;
  const quiz = await Assessment.findOne({ _id: quizId });

  // for (var i = 0; i < answers.length; i++) {
  //   const questionIdExist = await Quiz.findOne({"questions._id": answers[i].questionId});

  //   if (questionIdExist && answers[i].answer === quiz.questions[i].correctOption) {
  //     answerSheet.score = answerSheet.score + quiz.questions[i].weightAge;
  //   }
  // }

  const optionIdExist = await Assessment.findOne({
    "questions.options._id": optionId
  }, {
    "questions.options.$": 1,
    _id: 0
  });

  optionIdExist.questions[0].options.forEach(option => {
    if (option._id == answer[i].optionId[i]) {
      answerSheet.score = answerSheet.score + option.weight * quiz.questions[i].weightAge;
    }
  });

  await answerSheet.save();
  return "successfully submitted";
};
exports.quizTimeOut = () => {
  return "test submitted";
};
// type in which question will get answer eg mcq ,text input
exports.answerMedium = async inputType => {
  const answerMedium = await AnswerMedium.create({ inputType: inputType });
  return answerMedium.inputType;
};
//list of possible input type for answer a question
exports.answerMediumList = async () => {
  const answerMediumList = await AnswerMedium.find();
  return answerMediumList;
};

exports.quizDelete = async quizId => {
  const quizDeleted = await Assessment.findByIdAndDelete({ _id: quizId }).catch(error => {
    throw new ERROR(400, {
      key: "quiz delete",
      message: error.message
    });
  });
  return {
    success: !!quizDeleted
  };
};
//to remove question
exports.deleteQuestion = async (questionId, quizId) => {
  const deletedQuestion = await Assessment.findByIdAndUpdate({
    _id: quizId
  }, {
    $pull: {
      questions: {
        _id: questionId
      }
    }
  }).catch(error => {
    throw new ERROR(400, {
      key: "question delete",
      message: error.message
    });
  });
  //  if (!deletedQuestion) {
  //     throw new ERROR(400, {
  //       key: "quiz ",
  //       message: "question not found"
  //     });
  //   }

  return {
    success: !!deletedQuestion
  };
};

exports.deleteOption = async (quizId, optionId, questionId) => {
  const deletedOption = await Assessment.findByIdAndUpdate({
    _id: quizId
  }, {
    $pull: {
      "questions.$[].options": {
        _id: optionId
      }
    }
  }).catch(error => {
    throw new ERROR(400, {
      key: "option delete",
      message: error.message
    });
  });

  return {
    success: !!deletedOption
  };
};



exports.editQuiz = async (quizId, quizEdit) => {
  const updates = {
    ...quizEdit
  };
  const quiz = await Assessment.findByIdAndUpdate({
    _id: quizId
  }, updates).catch(error => {
    throw new ERROR(400, {
      key: "quiz update",
      message: error.message
    });
  });
  if (!quiz) {
    throw new ERROR(400, {
      key: "quiz update",
      message: "quiz not found"
    });
  }

  return {
    success: !!quiz
  };
};

exports.deleteQuiz = async quizId => {
  const quiz = await Assessment.findByIdAndDelete({ _id: quizId }).catch(error => {
    throw new ERROR(400, {
      key: "quiz update",
      message: error.message
    });
  });
  if (!quiz) {
    throw new ERROR(400, {
      key: "quiz update",
      message: "quiz not found"
    });
  }

  return {
    success: !!quiz
  };
};
