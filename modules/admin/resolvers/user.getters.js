const AuthService = require("../services/user.services");
module.exports = {
  getUser: async (parent, { userId }) => await AuthService.getUser(userId),
  roleList: async parent => await AuthService.roleList(),
  userList: async (parent, filter) => await AuthService.userList(filter),
  getRole: async (parent, { roleId }) => await AuthService.getRole(roleId),
  userId: (parent, { token }) => AuthService.token(token)
};
