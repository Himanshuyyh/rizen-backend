const getters = require("./user.getters");
const mutations = require("./user.mutations");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
