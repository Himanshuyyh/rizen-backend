const AuthService = require("../services/user.services");

module.exports = {
  login: async (parent, { data }) => await AuthService.adminLogin(data),
  roleCreate: async (parent, { permission, name }) => await AuthService.createRole(permission, name),
  userCreate: async (parent, { data, roleId }) => await AuthService.createUser(data, roleId),
  editUser: async (parent, { data, roleId, userId }) => await AuthService.editUser(data, roleId, userId),
  editRole: async (parent, { permission, name, roleId }) => await AuthService.editRole(permission, name, roleId),
  deleteRole: async (parent, { roleId }) => await AuthService.deleteRole(roleId),
  deleteUser: async (parent, { userId }) => await AuthService.deleteUser(userId)
};
