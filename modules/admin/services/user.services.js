const User = require("../../../models/user");
const Role = require("../../../models/role");
const bcrypt = require("bcrypt");
const { ERROR } = require("../../../helpers/errors");
const jwt = require("jsonwebtoken");
exports.show = async userId => {
  return User.findOne({ _id: userId });
};

exports.getUser = async id => {
  const user = await User.findById(id);
  if (!user)
    throw new ERROR(400, {
      key: "user",
      message: "user does not exist"
    });

  return user;
};
exports.userList = async filter => {
  const {
    skip = 0,
    limit = 10
  } = filter;
  const user = await User.find().skip(skip).limit(limit);
  return user;
};
exports.roleList = async () => {
  const role = await Role.find();
  return role;
};

exports.getRole = async roleId => {
  const role = await Role.findOne({ _id: roleId });
  if (!role)
    throw new ERROR(400, {
      key: "role",
      message: "role does not exist"
    });

  return role;
};

exports.createRole = async (permission, name) => {
  const role = new Role({ name: name, permission });
  await role.save();
  return role;
};

exports.editRole = async (permission, name, roleId) => {
  const role = await Role.findByIdAndUpdate({
    _id: roleId
  }, {
    permission: permission,
    name: name
  });

  return {
    success: !!role
  };
};

exports.editUser = async (data, roleId, userId) => {
  const { email, password } = data;
  const user = await User.findById({ _id: userId });
  user.email = email;
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    user.password = password;
  }

  user.roleId = roleId;
  await user.save();
  return {
    success: !!user
  };
};

exports.createUser = async (data, roleId) => {

  const userEmailExist = await User.findOne({ email: data.email });
  if (userEmailExist) {
    throw new ERROR(400, {
      key: "user",
      message: "User email exist"
    });
  }

  const user = await User.create({ ...data, roleId });
  return {
    success: !!user,
    id: user._id
  };
};

exports.adminLogin = async data => {
  const { email, password } = data;
  //check if email ad password exists of admin
  const user = await User.findByCredentials(email, password);

  if (!user) {
    throw new ERROR(400, {
      key: "Admin",
      message: "unable to login"
    });
  }
  const token = jwt.sign({
    userId: user._id
  }, process.env.CLIENT_SECRET, { expiresIn: "1h" });
  user.token = token;
  return { id: user._id, token: token };
};
// to verify the routes
exports.token = async token => {
  decodedToken = jwt.verify(token, process.env.CLIENT_SECRET);
  return decodedToken.userId;
};
exports.deleteUser = async userId => {
  const user = await User.findByIdAndDelete({ _id: userId }).catch(error => {
    throw new ERROR(400, {
      key: " user",
      message: error.message
    });
  });
  return {
    success: !!user
  };
};
exports.deleteRole = async roleId => {
  const role = await Role.findByIdAndDelete({ _id: roleId }).catch(error => {
    throw new ERROR(400, {
      key: " user",
      message: error.message
    });
  });
  return {
    success: !!role
  };
};
