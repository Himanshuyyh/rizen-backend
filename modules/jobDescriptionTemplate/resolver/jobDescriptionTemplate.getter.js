const jobDescriptionTemplateService = require('../service/jobDescriptionTemplate.services')
module.exports = {
    jobDescriptionTemplateList: async (parent, { filter, searchValue }, { USER }) => await jobDescriptionTemplateService.jobDescriptionTemplateList(filter, searchValue, USER._id),
    getJobDescription: async (parent, { jobDescriptionId }, { USER }) => await jobDescriptionTemplateService.getJobDescriptionTemplate(jobDescriptionId, USER._id),
    jdKeywordsList: async (parent) => await jobDescriptionTemplateService.jdKeywordsList(),
    skillList: async (parent) => await jobDescriptionTemplateService.skillList(),
    jdLocationList: async (parent) => await jobDescriptionTemplateService.jdLocationList()
};
