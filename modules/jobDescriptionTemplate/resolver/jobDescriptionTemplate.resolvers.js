const getters = require("./jobDescriptionTemplate.getter");
const mutations = require("./jobDescriptionTemplate.mutations");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
