const jobDescriptionTemplateService = require("../service/jobDescriptionTemplate.services");
module.exports = {
  jobDescriptionCreate: (parent, { jobDescriptionData }, { USER }) => jobDescriptionTemplateService.jobDescriptionTemplateCreate(jobDescriptionData, USER._id),
  jobDescriptionEdit: (parent, { jobDescriptionEdit, jobDescriptionId }, { USER }) => jobDescriptionTemplateService.jobDescriptionTemplateEdit(jobDescriptionEdit, jobDescriptionId, USER._id),
  jobDescriptionDelete: (parent, { jobDescriptionId }, { USER }) => jobDescriptionTemplateService.jobDescriptionTemplateDelete(jobDescriptionId, USER._id),
  addJdKeyword: (parent, { keyword }) => jobDescriptionTemplateService.addJdKeyword(keyword),
  addLocation: (parent, { location }) => jobDescriptionTemplateService.addLocation(location),

};
