const JobDescriptionTemplate = require("../../../models/JobDescriptionTemplate");
const JdKeywords = require("../../../models/jdKeywords");
const logger = require("../../../logger");
const Location = require('../../../models/location')
const Skill = require("../../../models/jdSkills");
const { ERROR } = require("../../../helpers/errors");
const Constants = require("../../../helpers/constants");

exports.jobDescriptionTemplateCreate = async (jobDescriptionData, userId) => {

  const jobDescriptionTemplate = new JobDescriptionTemplate({
    ...jobDescriptionData, createdBy: userId
  });
  if (jobDescriptionData.status === Constants.statusType.published) {
    if (plannedDate) {
      jobDescriptionTemplate.jobDate = jobDate;
    }
  }
  await jobDescriptionTemplate.save();
  return {
    success: !!jobDescriptionTemplate
  };
};
// to edit jd
exports.jobDescriptionTemplateEdit = async (jobDescriptionTemplateEdit, jobDescriptionTemplateId) => {
  const { updatedBy, plannedDate, jobDate, status } = jobDescriptionTemplateEdit;
  const updates = {
    ...jobDescriptionTemplateEdit
  };
  if (status === Constants.statusType.published) {
    if (plannedDate) {
      updates.jobDate = jobDate;
    }
  }
  if (updatedBy)
    updates.updatedBy = updatedBy;
  const jobDescriptionTemplate = await JobDescriptionTemplate.findOneAndUpdate({
    _id: jobDescriptionTemplateId
  }, updates).catch(error => {
    throw new ERROR(400, {
      key: "jd update",
      message: error.message
    });
  });
  if (!jobDescriptionTemplate) {
    throw new ERROR(400, {
      key: "jd update",
      message: "jd not found"
    });
  }
  logger.info("jd : Successfully edited.");
  return {
    success: !!jobDescriptionTemplate
  };
};
// to delete template
exports.jobDescriptionTemplateDelete = async jobDescriptionTemplateId => {
  const jobDescriptionTemplate = await JobDescriptionTemplate.findByIdAndDelete({ _id: jobDescriptionTemplateId }).catch(error => {
    throw new ERROR(400, {
      key: "jd delete",
      message: error.message
    });
  });
  return {
    success: !!jobDescriptionTemplate
  };
};

exports.jobDescriptionTemplateList = async (data, searchValue) => {
  const {
    skip = 0,
    limit = 10,
    sortName = "updatedAt",
    sortType = -1
  } = data;
  const count = await JobDescriptionTemplate.find().countDocuments().catch(error => {
    throw new ERROR(400, {
      key: "jd list",
      message: error.message
    });
  });

  if (searchValue) {
    const jobDescriptionTemplate = await JobDescriptionTemplate.find({
      $text: {
        $search: searchValue
      },

    }).skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
      throw new ERROR(400, {
        key: "jd list",
        message: error.message
      });
    })
    return {
      jobDescriptionTemplate,
      count
    }
  }
  const jobDescriptionTemplate = await JobDescriptionTemplate.find().skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
    throw new ERROR(400, {
      key: "jd list",
      message: error.message
    });
  });
  return {
    jobDescriptionTemplate,
    count
  };
};
exports.getJobDescriptionTemplate = async jobDescriptionId => {
  const jobDescriptionTemplate = await JobDescriptionTemplate.findOne({ _id: jobDescriptionId }).catch(error => {
    throw new ERROR(400, {
      key: "get jd",
      message: error.message
    });

  });;
  return jobDescriptionTemplate;
};
// to add keyword in jd
exports.addJdKeyword = async keyword => {
  const addedKeywords = await JdKeywords.create({ keyword: keyword });

  return addedKeywords.keyword;
};
exports.addLocation = async location => {

  const addedLocation = await Location.create({ location: location });

  return addedLocation.location;
};
//to give list of keywords used in searching
exports.jdKeywordsList = async () => {
  const list = await JdKeywords.find({}, {
    keyword: 1,
    _id: 0
  });
  return list;
};
//list of skills to select in jd
exports.skillList = async () => {
  const list = await Skill.find({}, { _id: 0 });
  // const list= await Country.insertMany([{countryName:"India"},{countryName:"India"},{countryName:"India"},{countryName:""}])
  return list;
};




exports.jdLocationList = async () => {
  // await  Location.insertMany([
  //     { location: 'Bangalore' },
  //     { location: 'Pune' },
  //     { location: 'Mumbai' },
  //     { location: 'Surat' },
  //     { location: 'Hyderabad' },
  //     { location: 'Ahmadabad' }
  // ])

  const list = await Location.find({}, {
    location: 1,
  });
  return list;
};
