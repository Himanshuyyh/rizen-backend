const Employee = require('../../../models/employee')
const { ERROR } = require("../../../helpers/errors");
//const imageUpload = require("@/helpers/image")
exports.employeeRegister = async (employeeData) => {
    const { dateOfBirth,
        fatherName, address,
        panID,
        AadharCardNo,
        mobileNumber,
        landLineNumber,
        passportNumber,
        emergencyContact,
        employeeQualification,
        departmentId, roleId } = employeeData
    //check if email exist
    // //const employeeEmailExist = await Employee.findOne({ where: { email: email } });
    // if (employeeEmailExist) {
    //     throw new ERROR(400, {
    //         key: "employee",
    //         message: "Employee email exist",
    //     });
    // }
    //let serverFile = null
    //if (file)
    //to upload image in public folder if file input is present
    // serverFile = await imageUpload(file)

    const employeeCreate = await Employee.create(data).catch(error => {
        throw new ERROR(400, {
            key: "employee create",
            message: error.message
        });
    });
    //     const employeeData = await Employee.sync({force:true})
    return { success: !!employeeCreate }






};


// exports.employeeRegister = async (employeeData, file) => {
//     const { name, email, age, departmentId } = employeeData;
//     //check if email exist
//     const employeeEmailExist = await Employee.findOne({
//         where: {
//             email: email
//         }
//     });
//     if (employeeEmailExist) {
//         throw new ERROR(400, {
//             key: "employee",
//             message: "Employee email exist"
//         });
//     }
//     let serverFile = null;
//     if (file)
//         //to upload image in public folder if file input is present
//         serverFile = await imageUpload(file);
//     const employeeRegister = await Employee.create({ name: name, age: age, email: email, profilePic: serverFile, departmentId: departmentId });
//     //     const employeeData = await Employee.sync({force:true})
//     return employeeRegister;
// };

// //to get list of all employees
// exports.employees = async data => {
//     const { limit, offset } = data;
//     const employees = await Employee.findAll({ limit: limit, offset: offset, include: Department });
//     console.log(employees[0].department);
//     return employees;
// };

// //to get employee from id
// exports.employee = async id => {
//     const employee = await Employee.findOne({
//         where: {
//             id: id
//         },
//         include: Department
//     });
//     return employee;
// };

// //get employee by department
// exports.employeeByDepartment = async (data, departmentId) => {
//     const { limit, offset } = data;
//     const employees = await Employee.findAll({
//         limit: limit, offset: offset, where: {
//             departmentId
//         }, include: Department
//     });
//     return employees;
// };
