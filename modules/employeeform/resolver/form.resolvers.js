const getters = require("./form.getter");
const mutations = require("./form.mutation");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
