const Department = require("../../../models/department");
const { ERROR } = require("../../../helpers/errors");
exports.createDepartment = async (data) => {
    const { name } = data
    const departmentExist = await Department.findOne({ name: name });
    if (departmentExist) {
        throw new ERROR(400, {
            key: "department",
            message: "Department already exist",
        });
    }
    const department = await Department.create({ name: name })
    return {
        success: !!department,
        id: department._id
    }
};
