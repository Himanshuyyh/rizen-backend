const getters = require("./department.getters");
const mutations = require("./department.mutations");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
