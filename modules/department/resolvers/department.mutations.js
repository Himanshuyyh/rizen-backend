const departmentService = require("../services/department.services");

module.exports = {
    createDepartment: async (parent, { data }) => await departmentService.createDepartment(data)
    
};
