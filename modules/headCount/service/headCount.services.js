const HeadCount = require("../../../models/headCount");
const { ERROR } = require("../../../helpers/errors");
const JobDescription = require("../../../models/JobDescription");
//to create head count
exports.headCountCreate = async data => {

  const headCount = await HeadCount.create(data).catch(error => {
    throw new ERROR(400, {
      key: "head count",
      message: error.message,

    });
  });
  return {
    success: !!headCount
  };
};
//list of head count
exports.headCountList = async (filter) => {
  const { skip = 0, limit = 10, sortName = "updatedAt", sortType = -1 } = filter
  const headCount = await
    HeadCount.find().populate('createdBy', { name: 1, _id: 0 }).skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
      throw new ERROR(400, {
        key: "head count list",
        message: error.message,
      });

    });
  const count = await HeadCount.find().countDocuments().catch(error => {
    throw new ERROR(400, {
      key: "jd list",
      message: error.message
    });
  });
  return {
    headCount,
    count
  }
}
exports.getHeadCount = async headCountId => await HeadCount.findById({ _id: headCountId }).catch(error => {
  throw new ERROR(400, {
    key: "head count ",
    message: error.message,

  });
});
exports.jobDescriptionList = async (filter, searchValue) => {
  const { skip = 0, limit = 10 } = filter

  const count = await JobDescription.find().countDocuments().skip(skip).limit(limit).catch(error => {
    throw new ERROR(400, {
      key: "job description list",
      message: error.message
    });
  });

  if (searchValue) {
    const jd = await JobDescriptionTemplate.find({
      $text: {
        $search: searchValue
      },

    }).skip(skip).limit(limit).sort({ [sortName]: sortType }).catch(error => {
      throw new ERROR(400, {
        key: "jd list",
        message: error.message
      });
    })
    return {
      jd,
      count
    }
  }
  const jd = await JobDescription.find().populate("jdTemplateId").populate("headCountId").populate(" assessmentId").skip(skip).limit(limit).catch(error => {
    throw new ERROR(400, {
      key: "job description list",
      message: error.message
    });
  });
  return {
    jd,
    count
  };

}
exports.getJobDescription = async (jdId) => await JobDescription.findById({ _id: jdId }).populate("jdTemplateId").populate("headCountId").populate(" assessmentId").catch(error => {
  throw new ERROR(400, {
    key: " get job description ",
    message: error.message
  });
});

exports.jobDescriptionDelete = async (jdId) => {
  return {
    success: !! await JobDescription.findByIdAndDelete({ _id: jdId }).catch(error => {
      throw new ERROR(400, {
        key: "job description delete",
        message: error.message

      });
    })
  }
};


exports.editHeadCount = async (headCountId, headCountEdit) => {
  const updates = {
    ...headCountEdit
  };
  const headCount = await HeadCount.findByIdAndUpdate({
    _id: headCountId
  }, updates).catch(error => {
    throw new ERROR(400, {
      key: "head count update",
      message: error.message
    });
  });
  if (!headCount) {
    throw new ERROR(400, {
      key: "head count update",
      message: "head count not found"
    });
  }

  return {
    success: !!headCount
  };
};

exports.editJd = async (jdId, jdEdit) => {
  const updates = {
    ...jdEdit
  };
  const jd = await JobDescription.findByIdAndUpdate({
    _id: jdId
  }, updates).catch(error => {
    throw new ERROR(400, {
      key: "jd update",
      message: error.message
    });
  });
  if (!jd) {
    throw new ERROR(400, {
      key: "jd update",
      message: "jd not found"
    });
  }

  return {
    success: !!jd
  };
};

exports.updateHeadCountStatus = async (headCountId, status) => {

  const headCount = await HeadCount.findByIdAndUpdate({
    _id: headCountId
  }, { status: status },
    { new: true }).catch(error => {
      throw new ERROR(400, {
        key: "head count update",
        message: error.message,

      });
    });
  if (!headCount) {
    throw new ERROR(400, {
      key: "head count update",
      message: "head count not found",


    });
  }

  return {
    success: !!headCount
  };
};


exports.headCountDelete = async (headCountId) => {
  const headCount = await HeadCount.findByIdAndDelete({ _id: headCountId }).catch(error => {
    throw new ERROR(400, {
      key: "headCount list",
      message: error.message
    });
  });
  console.log(headCount)
  return { success: !!headCount }
}

exports.jobDescriptionCreate = async data => {


  const jobDescription = await JobDescription.create(data).catch(error => {
    throw new ERROR(400, {
      key: "head count",
      message: error.message
    });
  });
  return {
    success: !!jobDescription
  };
};
