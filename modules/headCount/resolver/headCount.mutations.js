const headCountServices = require("../service/headCount.services");
module.exports = {
  headCountCreate: (parent, { data }) => headCountServices.headCountCreate(data),
  jdCreate: (parent, { data }) => headCountServices.jobDescriptionCreate(data),
  jdDelete: (parent, { jdId }) => headCountServices.jobDescriptionDelete(jdId),
  headCountDelete: (parent, { headCountId }) => headCountServices.headCountDelete(headCountId),
  editHeadCount: (parent, { headCountId, headCountEdit }) => headCountServices.editHeadCount(headCountId, headCountEdit),
  updateHeadCountStatus: (parent, { headCountId, status }) => headCountServices.updateHeadCountStatus(headCountId, status),
  editJd: (parent, { jdId, jdEdit }) => headCountServices.editJd(jdId, jdEdit)
};
