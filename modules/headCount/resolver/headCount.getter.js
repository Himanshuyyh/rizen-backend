const headCountService = require("../service/headCount.services");
module.exports = {
  headCountList: (parent, { filter }) => headCountService.headCountList(filter),
  getHeadCount: (parent, { headCountId }) => headCountService.getHeadCount(headCountId),
  jdList: (parent, { filter, search }) => headCountService.jobDescriptionList(filter, search),
  getjd: (parent, { jdId }) => headCountService.getJobDescription(jdId)
};
