const getters = require("./headCount.getter");
const mutations = require("./headCount.mutations");

module.exports = {
    Query: getters,
    Mutation: mutations,
};
