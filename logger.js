const log4js = require('log4js');
const jsonStringifySafe = require('json-stringify-safe');
const util = require('util');

log4js.addLayout('ndjson', () => (logEvent) => {
    const replacer = (key, value) => value;
    const data = logEvent.data.map(x => (typeof x === 'string' ? x : jsonStringifySafe(x, replacer)));
    return `${logEvent.level}: ${util.format(...data)}`;
});

log4js.configure({
    appenders: {
        out: {
            type: 'stdout',
            layout: {
                type: 'ndjson',
            },
        },
    },
    categories: {
        default: {
            appenders: ['out'],
            level: 'debug',
        },
    },
});

module.exports = log4js.getLogger();
