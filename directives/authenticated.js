const { SchemaDirectiveVisitor } = require("@graphql-tools/utils");
const { defaultFieldResolver } = require("graphql");
const AuthMiddleware = require("../middlewares/auth");
const { ERROR } = require("../helpers/errors");
//const Constants = require("../helpers/constants");

class AuthenticatedDirective extends SchemaDirectiveVisitor {
    visitFieldDefinition(_field, _details) {
        const { resolve = defaultFieldResolver } = _field;

        _field.resolve = async (...args) => {
            const [parent, data, context] = args;
            const user = await AuthMiddleware(context)
            if (!user) {
                
                throw new ERROR(401, null, "Unauthenticated Access");
            }
            context.USER = user;
            return resolve.apply(this, args);
        };
    }
}

module.exports = AuthenticatedDirective;
