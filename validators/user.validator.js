const Joi = require("joi");

const { string, email, mobile, any,number } = require("@/helpers/validators");

const Shared = {
    name: string.optional(),
    username: string.optional(),
    mobile: mobile.optional(),
    email: email.optional(),
    gender: string.optional(),
    dob: string.optional(),
    latitude: string.optional(),
    longitude: string.optional(),
    city: string.optional(),
    location:string.optional(),
    state: string.optional(),
    colorCode: string.optional(),
    version: string.optional(),
    type: string.optional(),
    image: any.optional(),
};

exports.store = Joi.object(Shared);

exports.update = Joi.object(Shared);

exports.update1 = Joi.object({image: {
        maxBytes: 20715200,
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data'
    }});
