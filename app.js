const path = require("path");
const express = require("express");
const cors = require('cors')
const dotenv = require("dotenv");

dotenv.config();
process.env.APPPATH = path.resolve(__dirname);
const app = express()
app.use(cors());

require("module-alias/register");
const ApolloServer = require("./config/graphql/apollo");
app.use(express.static(path.join(__dirname, './public')));
ApolloServer.applyMiddleware({ app });


require('./db/sequalize')
//db.authenticate().then(()=>console.log('db connected')).catch((err)=>console.log(err))

module.exports = app