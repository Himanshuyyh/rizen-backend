
const Sequelize = require('sequelize');

const mongoose = require('mongoose')

mongoose.connect(
     process.env.MONGODB_URL, {
     useNewUrlParser: true,
     useFindAndModify: false
 }
 ).then(() => console.log('Connected to database...'));


// module.exports= new Sequelize(process.env.dbName, process.env.usename, process.env.dbpassword, {
//     host: 'localhost',
//     dialect: 'postgres' 
//   }, {pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000
//   }});